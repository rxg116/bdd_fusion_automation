ARG PROXY
FROM ${PROXY}markhobson/maven-chrome:jdk-17

COPY settings.xml /usr/share/maven/ref/
COPY license.bin /usr/licenses/
#NGTP looks for webdriver in System property user.dir and folder 'webdrivers'
RUN mkdir usr/webdrivers
#ADD /usr/bin/chromedriver /usr/webdrivers

ENV LICENSE_PATH=/usr/licenses/license.bin

